﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HUD : MonoBehaviour {

    public Level level;
    public GameOver gameOver;

    public Text remainingText;
    public Text remainingSubText;
    public Text targetText;
    public Text targetSubtext;
    public Text scoreText;
    public Image[] stars;

    private int starIdx = 0;

	// Use this for initialization
	void Start () {
        SetVisibleStar(starIdx);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetScore(int score)
    {
        scoreText.text = score.ToString();

        int visibleStar = 0;

        if (score >= level.score3Star)
            visibleStar = 3;
        else
            if (score >= level.score2Star)
            visibleStar = 2;
        else
            if (score >= level.score1Star)
            visibleStar = 1;

        SetVisibleStar(visibleStar);

        starIdx = visibleStar;

    }

    private void SetVisibleStar(int index)
    {
        for (int i = 0; i < stars.Length; i++)
        {
            if (i == index)
            {
                stars[i].enabled = true;
            }
            else
                stars[i].enabled = false;
        }
    }

    public void SetTarget(int target)
    {
        targetText.text = target.ToString();
    }

    public void SetRemaining(int remaining)
    {
        remainingText.text = remaining.ToString();
    }

    public void SetRemaining(string remaining)
    {
        remainingText.text = remaining;
    }

    public void SetLevelType(Level.LevelType type)
    {
        if (type == Level.LevelType.MOVES)
        {
            remainingSubText.text = "moves remaining";
            targetSubtext.text = "target score";
        }else
            if (type == Level.LevelType.OBSTACLE)
        {
            remainingSubText.text = "moves remaining";
            targetSubtext.text = "bubbles remaining";
        }else
            if (type == Level.LevelType.TIMER)
        {
            remainingSubText.text = "time remaining";
            targetSubtext.text = "target score";
        }
    }

    public void OnGameWin(int score)
    {
        gameOver.ShowWin(score, starIdx);

        if (starIdx > PlayerPrefs.GetInt(SceneManager.GetActiveScene().name, 0))
        {
            PlayerPrefs.SetInt(SceneManager.GetActiveScene().name, starIdx);
        }
    }


    public void OnGameLose()
    {
        gameOver.ShowLose();
    }
}
