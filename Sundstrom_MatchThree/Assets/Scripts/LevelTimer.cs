﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelTimer : Level {

    public int timeInSeconds;
    public int targetScore;

    private float timer;

    public bool timeOut = false;

	// Use this for initialization
	void Start () {
        type = LevelType.TIMER;

        hud.SetLevelType(type);
        hud.SetScore(currentScore);
        hud.SetTarget(targetScore);
        hud.SetRemaining(string.Format("{0}:{1:00}", timeInSeconds / 60f, timeInSeconds % 60f));
        
	}
	
	// Update is called once per frame
	void Update () {
        if (timeOut)
            return;

        timer += Time.deltaTime;
        hud.SetRemaining(string.Format("{0}:{1:00}", (int)Mathf.Max((timeInSeconds- timer) / 60f, 0f), (int)Mathf.Max((timeInSeconds- timer) % 60f), 0f));

        if (timeInSeconds - timer <= 0) {
            timeOut = true;
            if (currentScore >= targetScore)
            {
                GameWin();
            }else
            {
                GameLose();
            }
        }
	}
}
